Create queue for each pagination page:

php create_tasks_for_each_page.php

Create customer, that parse single pagination page and create queue for parsing each message:

php handle_per_page_queue.php

Create customer for saving each post message:

php handle_single_post.php



PostgreSQL stored procedure:

CREATE FUNCTION InsertPost(subject varchar, date varchar, author varchar, message text)
  RETURNS void AS
  $BODY$
      BEGIN
        INSERT INTO posts(subject, date, author, message)
        VALUES(subject, date, author, message);
      END;
  $BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;