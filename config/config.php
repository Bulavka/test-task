<?php

// DB config
const DB_HOST = 'localhost';
const DB_USER = 'postgres';
const DB_PASSWORD = '123';
const DB_PORT = '5432';
const DB_NAME = 'posts';


// RabbitMQ config

const RABBITMQ_HOST = 'localhost';
const RABBITMQ_PORT = '5672';
const RABBITMQ_USER = 'guest';
const RABBITMQ_PASSWORD = 'guest';

// Other config
const RESOURCE_URL = 'https://forumodua.com';
const RESOURCE_LOGIN_PART = 'login.php?do=login';
const RESOURCE_LOGIN_URL = RESOURCE_URL . '/'.RESOURCE_LOGIN_PART;
const THEME_URL_PART = 'showthread.php?t=22715';
const THEME_URL = RESOURCE_URL . '/' . THEME_URL_PART;
const LOGIN_DATA = 'vb_login_username=Test+guest&vb_login_md5password=452668e3fe6ae4733dda960d9229b646&vb_login_md5password_utf=452668e3fe6ae4733dda960d9229b646';
const COOKIE_PATH = 'cookie/cookie.txt';
const PARSE_PAGES_LIMIT = 10;



