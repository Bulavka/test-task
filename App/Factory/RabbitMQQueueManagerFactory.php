<?php

namespace App\Factory;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use App\QueueManager\RabbitMQQueueManager;

class RabbitMQQueueManagerFactory
{
    public function getManager(
        $host = 'localhost',
        $port = 5672,
        $user = 'guest',
        $password = 'guest'
    ):RabbitMQQueueManager  {
        return new RabbitMQQueueManager(
            new AMQPStreamConnection(
                $host,
                $port,
                $user,
                $password,
                '/',
                false,
                'AMQPLAIN',
                null,
                'en_US',
                60,
                60
            )
        );
    }
}