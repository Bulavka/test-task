<?php

namespace App\Curl;

use App\Exception\CurlException;
use App\Response\CurlResponse;

class CurlConnection
{
    /**
     * @var resource
     */
    protected $connection;

    public function __construct()
    {
        $this->initConnection();
    }

    protected function initConnection()
    {
        $this->connection = curl_init();

        curl_setopt(
            $this->connection,
            CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'
        );

        $headers[] = "Connection: keep-alive";
        $headers[] = "Accept: */*";

        curl_setopt($this->connection,CURLOPT_CONNECTTIMEOUT,10);
        curl_setopt($this->connection,CURLOPT_HEADER,true);
        curl_setopt($this->connection, CURLOPT_HTTPHEADER,  $headers);
        curl_setopt($this->connection, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->connection, CURLOPT_FOLLOWLOCATION, 1);
    }

    /**
     * @param string $url
     * @return CurlResponse
     */
    public function getResponse(string $url):CurlResponse
    {
        curl_setopt($this->connection, CURLOPT_URL, $url);
        $response = curl_exec($this->connection);
        $responseCode = (int)curl_getinfo($this->connection, CURLINFO_HTTP_CODE);
        $headerLength = curl_getinfo($this->connection, CURLINFO_HEADER_SIZE);
        $responseBody = substr($response, $headerLength);

        return new CurlResponse($responseCode, $responseBody);
    }

}