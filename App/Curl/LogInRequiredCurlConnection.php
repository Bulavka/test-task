<?php

namespace App\Curl;

use App\Exception\CurlException;

class LogInRequiredCurlConnection extends CurlConnection
{
    private const COOKIE_PATH = '/cookie/cookie.txt';

    private $loginUrl;

    private $postData;

    private $cookiePath;

    /**
     * LogInRequiredCurlConnection constructor.
     * @param string $loginUrl
     * @param string $postData
     * @throws CurlException
     */
    public function __construct(string $loginUrl, string $postData, string $cookiePath)
    {
        parent::__construct();
        $this->loginUrl = $loginUrl;
        $this->postData = $postData;
        $this->cookiePath = $cookiePath;
        $this->logIn();
    }

    /**
     * @param string $loginUrl
     * @param string $postData
     * @throws CurlException
     */
    public function logIn():void
    {

        curl_setopt($this->connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->connection, CURLOPT_SSL_VERIFYHOST, 0);
        //curl_setopt($this->connection, CURLOPT_REFERER, 'https://forumodua.com');
        //curl_setopt($this->connection, CURLOPT_VERBOSE, 1);
        curl_setopt($this->connection, CURLOPT_URL, $this->loginUrl);
        curl_setopt($this->connection, CURLOPT_POST, 1 );
        curl_setopt($this->connection, CURLOPT_POSTFIELDS, $this->postData);
        curl_setopt($this->connection, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($this->connection, CURLOPT_COOKIEFILE, $this->cookiePath);

        curl_exec($this->connection);

        $responseCode = (int)curl_getinfo($this->connection, CURLINFO_HTTP_CODE);

        if ($responseCode !== 200) {
            throw new CurlException('Login failed');
        }
    }
}