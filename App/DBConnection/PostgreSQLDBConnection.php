<?php

namespace App\DBConnection;


class PostgreSQLDBConnection implements DBConnectionInterface
{
    private $connection;

    public function __construct(string $host, string $port, string $user, string $pass, string $dbName)
    {
        $this->connection = pg_connect($this->createConnectString($host, $port, $user, $pass, $dbName));
    }

    private function createConnectString(string $host, string $port, string $user, string $pass, string $dbName):string
    {
        return "host=$host port=$port user=$user password=$pass dbname=$dbName ";
    }

    public function insert(array $data):void
    {
        $subject = $data['subject'];
        $date = $data['date'];
        $author = $data['author'];
        $message = $data['message'];

        $this->execute("SELECT * From InsertPost('$subject', '$date', '$author', '$message')");
    }

    public function execute(string $query):void
    {
        pg_query($this->connection, $query);
    }
}