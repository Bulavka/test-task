<?php

namespace App\DBConnection;

interface DBConnectionInterface
{
    public function insert(array $data):void;

    public function execute(string $query):void;
}