<?php

namespace App\Response;

class CurlResponse
{
    private const VALID_RESPONSE_CODE = 200;
    /**
     * @var int
     */
    private $code;
    /**
     * @var string
     */
    private $body;

    public function __construct(int $code, string $body)
    {
        $this->code = $code;
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getBody():string
    {
        return $this->body;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    public function isValid()
    {
        return $this->code === self::VALID_RESPONSE_CODE;
    }

}