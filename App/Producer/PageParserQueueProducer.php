<?php

namespace App\Producer;

use App\DataProvider\DataProviderInterface;
use App\DTO\AnchorDTO;
use App\QueueManager\QueueManagerInterface;

class PageParserQueueProducer implements ProducerInterface
{

    public const QUEUE_NAME = 'parse_pages_queue';
    /**
     * @var DataProviderInterface
     */
    protected $urlProvider;
    /**
     * @var QueueManagerInterface
     */
    protected $queueManager;
    /**
     * @var $url
     */
    protected $url;
    /**
     * @var int
     */
    private $pagesLimit;

    public function __construct(
        DataProviderInterface $urlProvider,
        QueueManagerInterface $queueManager,
        string $url,
        int $pagesLimit
    ){
        $this->urlProvider = $urlProvider;
        $this->queueManager = $queueManager;
        $this->url = $url;
        $this->pagesLimit = $pagesLimit;
    }

    public function produce(): void
    {
        /**
         * @var $urls AnchorDTO[]
         */
        $urls = $this->urlProvider->getData($this->url, $this->pagesLimit);

        if (!empty($urls)) {
            $this->queueManager->declareQueue(self::QUEUE_NAME);
            foreach ($urls as $url) {
                $this->queueManager->addToQueue($url->serialize(), self::QUEUE_NAME);
            }
        }

        $this->queueManager->closeConnection();
    }
}