<?php

namespace App\Producer;

interface ProducerInterface
{
    public function produce():void;
}