<?php


namespace App\Producer;

use App\DTO\TopicMessageAbstractDTO;
use App\QueueManager\QueueManagerInterface;

class PostsQueueProducer implements ProducerInterface
{
    public const QUEUE_NAME = 'parse_posts_queue';
    /**
     * @var QueueManagerInterface
     */
    private $queueManager;
    /**
     * @var TopicMessageAbstractDTO[]
     */
    private $posts;

    public function __construct(QueueManagerInterface $queueManager)
    {
        $this->queueManager = $queueManager;
    }

    public function produce(): void
    {
        $this->queueManager->declareQueue(self::QUEUE_NAME);

        foreach ($this->posts as $post) {
            $this->queueManager->addToQueue($post->serialize(), self::QUEUE_NAME);
        }

        $this->queueManager->closeConnection();
    }

    /**
     * @param TopicMessageAbstractDTO[] $posts
     * @return PostsQueueProducer
     */
    public function setPosts(array $posts): PostsQueueProducer
    {
        $this->posts = $posts;
        return $this;
    }
}