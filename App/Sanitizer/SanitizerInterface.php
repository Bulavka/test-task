<?php

namespace App\Sanitizer;

interface SanitizerInterface
{
    public function sanitize(string $str):string ;
}