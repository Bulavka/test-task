<?php

namespace App\Sanitizer;

class Sanitizer implements SanitizerInterface
{
    public function sanitize(string $str): string
    {
        $str = stripslashes($str);
        $str = htmlentities($str);
        $str = strip_tags($str);

        return trim($str);
    }
}