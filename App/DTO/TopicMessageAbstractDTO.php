<?php

namespace App\DTO;

class TopicMessageAbstractDTO implements SerializableDTOInterface
{
    private $author;

    private $subject;

    private $date;

    private $message;

    /**
     * @param mixed $author
     * @return TopicMessageAbstractDTO
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $subject
     * @return TopicMessageAbstractDTO
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $date
     * @return TopicMessageAbstractDTO
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $message
     * @return TopicMessageAbstractDTO
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return json_encode(
            [
                'subject' => $this->subject,
                'date' => $this->date,
                'author' => $this->author,
                'message' => $this->message
            ]
        );
    }

}