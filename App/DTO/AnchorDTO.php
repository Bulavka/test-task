<?php

namespace App\DTO;

class AnchorDTO implements SerializableDTOInterface
{
    private $link;

    /**
     * @param mixed $link
     * @return AnchorDTO
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return json_encode(
            ['link' => $this->link]
        );
    }
}