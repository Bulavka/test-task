<?php

namespace App\DTO;

interface SerializableDTOInterface
{
    public function serialize():string ;
}