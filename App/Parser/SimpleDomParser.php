<?php

namespace App\Parser;

use KubAT\PhpSimple\HtmlDomParser;
use simple_html_dom\simple_html_dom;

class SimpleDomParser extends HtmlDomParser
{
    /**
     * @var simple_html_dom
     */
    private $dom;

    public function load($html)
    {
        $this->dom = self::str_get_html($html);
    }

    /**
     * @return simple_html_dom
     */
    public function getDom(): simple_html_dom
    {
        return $this->dom;
    }

}