<?php

namespace App\Customer;

use App\DataProvider\TopicMessagesDataProvider;
use PhpAmqpLib\Message\AMQPMessage;
use App\Producer\PageParserQueueProducer;
use App\Producer\PostsQueueProducer;
use App\QueueManager\RabbitMQQueueManager;

class PageParserQueueCustomer implements CustomerInterface
{
    /**
     * @var RabbitMQQueueManager
     */
    private $queueManager;
    /**
     * @var TopicMessagesDataProvider
     */
    private $dataProvider;
    /**
     * @var PostsQueueProducer
     */
    private $producer;

    /**
     * PageParserQueueCustomer constructor.
     * @param RabbitMQQueueManager $queueManager
     * @param TopicMessagesDataProvider $dataProvider
     * @param PostsQueueProducer $producer
     * @throws \ErrorException
     */
    public function __construct(
        RabbitMQQueueManager $queueManager,
        TopicMessagesDataProvider $dataProvider,
        PostsQueueProducer $producer
    ) {
        $this->queueManager = $queueManager;
        $this->dataProvider = $dataProvider;
        $this->producer = $producer;
    }

    /**
     * @throws \ErrorException
     */
    public function listen():void
    {
        $this->queueManager->declareQueue(PageParserQueueProducer::QUEUE_NAME);
        $channel = $this->queueManager->getChannel();
        $channel->basic_consume(
            PageParserQueueProducer::QUEUE_NAME,
            '',
            false,
            true,
            false,
            false,
            array($this, 'proceed')
        );

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $this->queueManager->closeConnection();
    }

    /**
     * @param AMQPMessage $message]
     */
    public function proceed(AMQPMessage $message):void
    {
        $messageBody = $message->getBody();

        $messageBody = json_decode($messageBody, true);

        $postDTOs = $this->dataProvider->getData($messageBody['link']);

        $this->producer->setPosts($postDTOs)->produce();
    }

}