<?php


namespace App\Customer;

use App\DBConnection\DBConnectionInterface;
use PhpAmqpLib\Message\AMQPMessage;
use App\Producer\PageParserQueueProducer;
use App\Producer\PostsQueueProducer;
use App\QueueManager\RabbitMQQueueManager;

class PostInsertCustomer implements CustomerInterface
{
    /**
     * @var RabbitMQQueueManager
     */
    private $queueManager;
    /**
     * @var DBConnectionInterface
     */
    private $dbConnection;

    public function __construct(RabbitMQQueueManager $queueManager, DBConnectionInterface $dbConnection)
    {
        $this->queueManager = $queueManager;
        $this->dbConnection = $dbConnection;
    }

    public function listen(): void
    {
        $this->queueManager->declareQueue(PostsQueueProducer::QUEUE_NAME);
        $channel = $this->queueManager->getChannel();
        $channel->basic_consume(
            PostsQueueProducer::QUEUE_NAME,
            '',
            false,
            true,
            false,
            false,
            array($this, 'proceed')
        );

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $this->queueManager->closeConnection();
    }

    /**
     * @param AMQPMessage $message
     */
    public function proceed(AMQPMessage $message)
    {
        $post = $message->getBody();

        $post = json_decode($post, true);

        $this->dbConnection->insert($post);
    }
}