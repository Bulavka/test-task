<?php

namespace App\QueueManager;

interface QueueManagerInterface
{
    /**
     * @param string $queueName
     */
    public function declareQueue(string $queueName):void;

    /**
     * @param string $message
     * @param string $queueName
     */
    public function addToQueue(string $message, string $queueName):void;

    public function closeConnection():void;

}