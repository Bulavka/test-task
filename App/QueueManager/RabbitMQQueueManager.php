<?php

namespace App\QueueManager;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQQueueManager implements QueueManagerInterface
{
    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    public function __construct(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;
    }

    public function addToQueue(string $message, string $queueName): void
    {
        $this->connection->channel()->basic_publish(
            new AMQPMessage($message),
            '',
            $queueName
        );
    }

    public function declareQueue(string $queueName): void
    {
        $this->connection->channel()->queue_declare(
            $queueName,
            false,
            false,
            false,
            true
        );
    }

    /**
     * @return \PhpAmqpLib\Channel\AMQPChannel
     */
    public function getChannel()
    {
        return $this->connection->channel();
    }

    /**
     * @throws Exception
     */
    public function closeConnection(): void
    {
        $this->connection->channel()->close();
        $this->connection->close();
    }

}