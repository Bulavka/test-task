<?php

namespace App\DataProvider;

use App\DTO\AnchorDTO;
use App\Response\CurlResponse;

class PageUrlProvider extends AbstractByUrlDataProvider
{
    /**
     * @param string $url
     * @param int $limit
     * @return AnchorDTO[]
     */
    public function getData($url, int $limit = 0): array
    {
        if ($limit === 0) {
            return [];
        }

        $response = $this->connection->getResponse($url);

        $lastPaginationPage = $this->getLastPaginationPage($response);

        $limit = $lastPaginationPage < $limit ? $lastPaginationPage : $limit;

        $anchors[] = $url;

        for ($i = 2; $i <= $limit; $i++) {
            $anchors[] = $url . '&page=' . $i;
        }

        $DTOs = [];

        foreach($anchors as $anchor) {
            $DTOs[] = (new AnchorDTO())->setLink($anchor);
        }

        return $DTOs;
    }

    /**
     * @param CurlResponse $response
     * @return int
     */
    private function getLastPaginationPage(CurlResponse $response): int
    {
        $lastPage = 0;

        $dom = $this->getDom($response);

        $paginationForms = $dom->find('form.pagination');

        if (!empty($paginationForms)) {
            $paginationForm = current($paginationForms);
            $lastPageLinks =  $paginationForm->find('span.first_last a');
            if (!empty($lastPageLinks)) {
                $lastPageHref = current($lastPageLinks)->href;
                $query = parse_url($lastPageHref, PHP_URL_QUERY);

                if (!empty($query)) {
                    parse_str(html_entity_decode($query), $result);
                    $lastPage = \array_key_exists('page', $result) && is_numeric($result['page'])
                        ? (int)$result['page'] : 0;

                }
            }
        }

        return $lastPage;
    }

}