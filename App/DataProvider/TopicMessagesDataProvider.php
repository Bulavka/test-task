<?php


namespace App\DataProvider;

use App\Sanitizer\SanitizerInterface;
use App\Curl\CurlConnection;
use App\DTO\TopicMessageAbstractDTO;
use App\Parser\SimpleDomParser;
use simple_html_dom\simple_html_dom_node;

class TopicMessagesDataProvider extends AbstractByUrlDataProvider
{
    /**
     * @var SanitizerInterface
     */
    protected $sanitizer;

    public function __construct(CurlConnection $connection, SimpleDomParser $parser, SanitizerInterface $sanitizer)
    {
        parent::__construct($connection, $parser);
        $this->sanitizer = $sanitizer;
    }
    /**
     * @param string $url
     * @param int $limit
     * @return TopicMessageAbstractDTO[]
     */
    public function getData(string $url, int $limit = 0): array
    {
        $response = $this->connection->getResponse($url);

        $dom = $this->getDom($response);

        $posts = $dom->find('ol#posts > li');

        if ($limit !== 0) {
            $posts = array_slice($posts, 0, $limit);
        }

        $topicMessageDTOs = [];

        $subject = '';

        foreach ($posts as $key => $post) {
            if (preg_match('/^post_[0-9]+$/i', $post->id)) {
                $subject = !empty($this->getSubject($post)) ? $this->getSubject($post) : $subject;

                $author = $this->getAuthor($post);
                $date = $this->getDate($post);
                $message = $this->getMessage($post);
                $topicMessageDTOs[] = (new TopicMessageAbstractDTO())
                    ->setSubject($subject)
                    ->setAuthor($author)
                    ->setDate($date)
                    ->setMessage($message);
            }
        }


        return $topicMessageDTOs;
    }

    private function getSubject(simple_html_dom_node $dom)
    {
        $subjectDom = $dom->find('h2.title');
        return $this->getInnerText($subjectDom);
    }

    private function getDate(simple_html_dom_node $dom)
    {
        $dateDom = $dom->find('.date');
        return $this->getInnerText($dateDom);
    }

    private function getMessage(simple_html_dom_node $dom)
    {
        $messageDom = $dom->find('.postcontent');
        return $this->getInnerText($messageDom);
    }

    private function getAuthor(simple_html_dom_node $dom)
    {
        $authorDom = $dom->find('a.username');
        return $this->getInnerText($authorDom);
    }

    /**
     * @param simple_html_dom_node[] $nodes
     * @return string
     */
    private function getInnerText(array $nodes)
    {
        $innerText = !empty($nodes) ? current($nodes)->plaintext : '';

        return $this->sanitizer->sanitize($innerText);
    }
}