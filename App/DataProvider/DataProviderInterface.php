<?php

namespace App\DataProvider;

use App\DTO\SerializableDTOInterface;

interface DataProviderInterface
{
    /**
     * @param string $url
     * @param int $limit
     * @return SerializableDTOInterface[]
     */
    public function getData(string $url, int $limit = 0): array;
}