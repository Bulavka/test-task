<?php


namespace App\DataProvider;

use App\Curl\CurlConnection;
use App\Parser\SimpleDomParser;
use App\Response\CurlResponse;
use simple_html_dom\simple_html_dom;

abstract class AbstractByUrlDataProvider implements DataProviderInterface
{
    /**
     * @var CurlConnection;
     */
    protected $connection;
    /**
     * @var SimpleDomParser
     */
    protected $parser;

    public function __construct(CurlConnection $connection, SimpleDomParser $parser)
    {
        $this->connection = $connection;
        $this->parser = $parser;
    }


    protected function getDom(CurlResponse $response): simple_html_dom
    {
        $this->parser->load($response->getBody());

        return $this->parser->getDom();
    }

}