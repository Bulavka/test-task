<?php

namespace App\DataProvider;

interface UrlProviderInterface
{
    /**
     * @param int $limit
     * @return array
     */
    public function getUrls(int $limit):array;
}