<?php
include 'config/config.php';
include 'autoload.php';

use App\Factory\RabbitMQQueueManagerFactory;
use App\Curl\LogInRequiredCurlConnection;
use App\DataProvider\PageUrlProvider;
use App\Parser\SimpleDomParser;
use App\Producer\PageParserQueueProducer;

$pageUrlProvider = new PageUrlProvider(
    new LogInRequiredCurlConnection(
        RESOURCE_LOGIN_URL,
        LOGIN_DATA,
        COOKIE_PATH
    ),
    new SimpleDomParser()
);

$rabbitMQManagerFactory = new RabbitMQQueueManagerFactory();
$rabbitMQManager = $rabbitMQManagerFactory->getManager(
    RABBITMQ_HOST,
    RABBITMQ_PORT,
    RABBITMQ_USER,
    RABBITMQ_PASSWORD
);


$producer = new PageParserQueueProducer($pageUrlProvider, $rabbitMQManager, THEME_URL, PARSE_PAGES_LIMIT);
$producer->produce();
exit();