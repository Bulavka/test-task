<?php

include 'vendor/autoload.php';

spl_autoload_register(function ($class_name) {
    $className = __DIR__ . '/' . strtr($class_name, '_\\', '//') . '.php';

    if (!file_exists($className)) {
        throw new Exception("Unable to load $className.");
    }

    require_once($className);
});
