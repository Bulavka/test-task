<?php

use App\DBConnection\PostgreSQLDBConnection;
use App\Factory\RabbitMQQueueManagerFactory;
use App\Customer\PostInsertCustomer;

include 'config/config.php';
include 'autoload.php';

$rabbitMQManagerFactory = new RabbitMQQueueManagerFactory();
$rabbitMQManager = $rabbitMQManagerFactory->getManager(
    RABBITMQ_HOST,
    RABBITMQ_PORT,
    RABBITMQ_USER,
    RABBITMQ_PASSWORD
);

$DBConnection = new PostgreSQLDBConnection(
    DB_HOST,
    DB_PORT,
    DB_USER,
    DB_PASSWORD,
    DB_NAME
);

$customer = new PostInsertCustomer(
    $rabbitMQManager,
    $DBConnection
);
$customer->listen();

die();
$conn = pg_connect('host=localhost port=5432 dbname=posts user=postgres password=123');
pg_query($conn, "SELECT * From InsertPost('aaa', 'fasfas', 'hfdhd', 'hdfhfdhf')");
