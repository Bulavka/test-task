<?php

include 'config/config.php';
include 'autoload.php';

use App\Factory\RabbitMQQueueManagerFactory;
use App\Sanitizer\Sanitizer;
use App\Curl\LogInRequiredCurlConnection;
use App\Customer\PageParserQueueCustomer;
use App\DataProvider\TopicMessagesDataProvider;
use App\Parser\SimpleDomParser;
use App\Producer\PostsQueueProducer;

$rabbitMQManagerFactory = new RabbitMQQueueManagerFactory();
$rabbitMQManager = $rabbitMQManagerFactory->getManager(
    RABBITMQ_HOST,
    RABBITMQ_PORT,
    RABBITMQ_USER,
    RABBITMQ_PASSWORD
);

$dataProvider = new TopicMessagesDataProvider(
    new LogInRequiredCurlConnection(
        RESOURCE_LOGIN_URL,
        LOGIN_DATA,
        COOKIE_PATH
    ),
    new SimpleDomParser(),
    new Sanitizer()
);


$customer = new PageParserQueueCustomer(
    $rabbitMQManager,
    $dataProvider,
    new PostsQueueProducer($rabbitMQManager)
);

$customer->listen();
